#ifndef NODE_H
#define NODE_H

#include "State.h"
#include "Position.h"
#include "help_funcs.h"

#include <vector>
#include <iostream>


using namespace std;

class Node
{
public:
    Node();
    //Node(State state);
    Node(vector<string> map);
    Node(Node *parent);
    Node(vector<string> map, Node *parent);

    Node(vector<string> map, Node *parent, vector<Position> box_goal); //Constructor to be used for A*


    //Getter functions:
    Node*               get_parent();
    vector<string>      get_map();
    vector<Position>    get_box_positions();
    Position            get_player_pos();
    int                 depth();            //Returns depts of node
    int                 get_g() const ;
    int                 get_h() const ;

    int                 calc_heuristic(vector<Position> goal);

    //Operator overloading
    //bool operator==(Node n2);

    //Closed list help:
    template<typename T> //https://stackoverflow.com/questions/972152/how-to-create-a-template-function-within-a-class-c
    bool in_list(T list)
    {
        for(int i = 0; i < list.size(); i++)
        {
            if(list[i] == *this)
                return true;
        }
        return false;
    }

    friend bool operator==(Node lhs, Node rhs) {
        vector<Position> lhs_b = lhs.get_box_positions();
        vector<Position> rhs_b = rhs.get_box_positions();

        bool box_pos_equal = false, player_pos_equal = false;

        box_pos_equal = equal(rhs_b.begin(), rhs_b.end(), lhs_b.begin());
        player_pos_equal = lhs.get_player_pos() == rhs.get_player_pos();

        //If boxes and player are in the same position, then the states are identitcal

        return box_pos_equal && player_pos_equal;
    }

private:
    Node* _parent; //Pointer to parent node

    //State:
    vector<string>      _map;
    Position            _player_pos;
    vector<Position>    _box_positions;
    int                 _g; //Path cost
    int                 _h; //Heuristic cost
};

//To be able to use a priority queue:
struct ComparisonClass {
    bool operator()(const Node& lhs, const Node& rhs)
    {
       return ( lhs.get_g() + lhs.get_h() ) > ( rhs.get_g() + rhs.get_h() );
    }
};



struct MyHash
{
    std::size_t operator()(Node n) const noexcept
    {
        vector<Position> bp = n.get_box_positions();
        Position pp = n.get_player_pos();

        string hash_string;

        hash_string.append("p" + to_string(pp._i) + "," + to_string(pp._j));
        for(auto box : bp)
        {
            hash_string.append("-b" + to_string(box._i) + "," + to_string(box._j));
        }

        std::size_t h = std::hash<std::string>{}(hash_string);
        return h;

//        vector<string> map = n.get_map();
//        std::size_t h1 = std::hash<std::string>{}(n.get_map()[0]);
//        std::size_t h2 = std::hash<std::string>{}(n.get_map()[1]);

//        return h1 ^ (h2 << 1); // or use boost::hash_combine (see Discussion)
    }
};

#endif // NODE_H
