#include <iostream>
#include <vector>

#include <stack>
#include <algorithm>    // std::equal
#include <queue>
#include <unordered_map> // Hash table

#include <time.h>


#include "Node.h"
#include "State.h"
#include "Position.h"

#include "Sokoban.h"

#include "glob_def.h"


using namespace std;
string print_directions(vector<vector<string>> maps);
vector<pos> deadlock_positions(vector<string> map);


int main()
{



    //example map:
    //vector<string> level = {"###", "#.#", "#@#", "#$#", "#G#", "###"};

    string str_level1 = "/home/soren/Robot/AI1/SS/levels/level1";
    string str_level2 = "/home/soren/Robot/AI1/SS/levels/level2";
    string str_level3 = "/home/soren/Robot/AI1/SS/levels/level3";
    string str_level4 = "/home/soren/Robot/AI1/SS/levels/level4";
    string str_level5 = "/home/soren/Robot/AI1/SS/levels/level5";
    string str_level6 = "/home/soren/Robot/AI1/SS/levels/level6";
    string str_comp =   "/home/soren/Robot/AI1/SS/levels/2018-competation-map";

    string str_comp_old  = "/home/soren/Robot/AI1/SS/levels/competition_old";


    vector<string> level1 = load_level_from_file(str_level1);
    vector<string> level2 = load_level_from_file(str_level2);
    vector<string> level3 = load_level_from_file(str_level3);
    vector<string> level4 = load_level_from_file(str_level4);
    vector<string> level5 = load_level_from_file(str_level5);
    vector<string> level6 = load_level_from_file(str_level6);

    vector<string> levelc_old = load_level_from_file(str_comp_old);
    vector<string> levelc = load_level_from_file(str_comp);


    vector<string> level = levelc;

    Node seed(level), goal;
    Sokoban *s;
    s = new Sokoban(level);
    //Sokoban s(level4);

     clock_t t = clock(); //Start timer
     //s->search(seed,goal);
     s->search_Astar(seed, goal);
     //s.search_IDFS(seed,goal);
     t = clock() - t;
     cout << "Found the goal in: " << (float)t / CLOCKS_PER_SEC << " seconds" << endl; //http://www.cplusplus.com/reference/ctime/clock/


    Node* nPtr = &goal;
    int idx = 0;

    vector<vector<string>> maps;
    while(nPtr != nullptr)
    {
        cout << "----------" << "" << idx << "---------" << endl;
        print_map(nPtr->get_map());
        maps.push_back(nPtr->get_map());
        nPtr = nPtr->get_parent();
        idx++;
    }


    cout << print_directions(maps);


    deadlock_positions(level3);

    return 0;
}


string print_directions(vector<vector<string>> maps)
{
    vector<pos> player_positions;
    for(auto& map : maps)
    {
        pos pp = find_player(map);
        player_positions.push_back(pp);
    }

    string directions;
    for(int i = player_positions.size() - 1; i > 0; i-- )
    {
        pos move = player_positions[i-1] - player_positions[i];
        vector<pos> box_pos_curr = find_boxes(maps[i]);
        vector<pos> box_pos_prev = find_boxes(maps[i-1]);

        bool box_is_pushed = false;
        if(! equal(box_pos_curr.begin(), box_pos_curr.end(), box_pos_prev.begin()) ) //If not equal, box has been pushed
           box_is_pushed = true;



        if(move == UP)
        {
            if(box_is_pushed)
                directions.append("U");
            else
                directions.append("u");
        }
        else if(move == DOWN)
        {
            if(box_is_pushed)
                directions.append("D");
            else
                directions.append("d");
        }

        else if(move == LEFT)
        {
            if(box_is_pushed)
                directions.append("L");
            else
                directions.append("l");
        }

        else
        {
            if(box_is_pushed)
                directions.append("R");
            else
                directions.append("r");
        }
    }

    return directions;

}

vector<pos> deadlock_positions(vector<string> map)
{
    vector<pos> deadlocks;

    for(int i = 1; i < map.size() - 1; i++)
    {
        for(int j = 1; j < map[i].size() - 1; j++)
        {
            if(map[i][j] != 'G')
            {
                if(map[i-1][j] == '#' && map[i][j-1] == '#')
                    deadlocks.push_back(pos(i,j));
                if(map[i-1][j] == '#' && map[i][j+1] == '#') //Nederst
                    deadlocks.push_back(pos(i,j));
                if(map[i+1][j] == '#' && map[i][j+1] == '#') //Neders højre
                    deadlocks.push_back(pos(i,j));
                if(map[i+1][j] == '#' && map[i][j-1] == '#') //Nederst venstre
                    deadlocks.push_back(pos(i,j));
            }

        }
    }

    return deadlocks;
}
