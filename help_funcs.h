#ifndef HELP_FUNCS_H
#define HELP_FUNCS_H

#include <vector>
#include <iostream>
#include <fstream>

#include "Position.h"

using namespace std;




Position find_player(vector<string> map);
vector<Position> find_boxes(vector<string> map);
vector<Position> find_goals(vector<string> map);
vector<Position> find_free_squares(vector<string> map);
void print_map(vector<string> map);

vector<string> load_level_from_file(string file_path);

//void print_path(Node goal);

//Functions dealing with the closed list:
bool in_list(pos p, vector<pos> list);



#endif // HELP_FUNCS_H
