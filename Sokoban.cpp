#include "Sokoban.h"

Sokoban::Sokoban(){}

Sokoban::Sokoban(vector<string> map)
{
    _map = map;

    _goal_pos = find_goals(map);

    /*
     * FORMAT: (Taken from http://www.sokobano.de/wiki/index.php?title=Level_format)
     *
     * @ - player
     * $ - box
     * G - goal
     * + - player on goal
     * * - box on goal
     *
     *
     *
     * indexing  (row,col) as with matrix.
     * NB. zero indexed.
     *
     *
     * ######
     * #.@..#
     * #....#
     * #.$..#
     * #.G..#
     * ######
     *
     */
    //sort(_goal_pos.begin(), _goal_pos.end(), comparePositions );
}


void Sokoban::search(Node init, Node &goal)
{
    _open_list.push(init);
    _closed_list.reserve(10000); //(8-11-2018 10:47) must be quite big for large mazes

    //queue<Node> open_list;
    Node node;
    vector<Node> children;

    while( !_open_list.empty() )
    {
        node = _open_list.front();
        _open_list.pop();
        _closed_list.push_back(node);
        _um.insert({node,true});

        if( is_goal(node) )
        {
            cout << "I found the goal!!!" << endl;
            goal = node;
            return;
        }
        //children = expand(node);
        children = expand(_closed_list.back()); //Expand the latest node from CL (to be able to reference)
        merge(children);
    }
}

vector<Node> Sokoban::expand(Node &node)
{
    vector<Node> children;

    vector<string> curr_map = node.get_map();
    vector<string> new_map = curr_map;

    pos pp = node.get_player_pos();

    //Look up:
    if( curr_map[pp._i - 1][pp._j] == '.' || curr_map[pp._i - 1][pp._j] == 'G' ) //If there is either a free spot or goal
    {
        if(pp.in_list(_goal_pos)) //If player is on a goal square, redraw the goalsquare, otherwise player must be on empty square '.'
            new_map[pp._i][pp._j] = 'G';
        else
            new_map[pp._i][pp._j] = '.';

        new_map[pp._i - 1][pp._j] = '@';
        Node n_up(new_map, &node);
        children.push_back(n_up);
    }
    else if( curr_map[pp._i - 1][pp._j] == '$'  ) //If there is a box
    {
        if( curr_map[pp._i - 2][pp._j] == '.' ||  curr_map[pp._i - 2][pp._j] == 'G' ) //Notice -2
        {
            if(pp.in_list(_goal_pos)) //If player is on a goal square, redraw the goalsquare, otherwise player must be on empty square '.'
                new_map[pp._i][pp._j] = 'G';
            else
                new_map[pp._i][pp._j] = '.';
            new_map[pp._i - 1][pp._j] = '@';
            new_map[pp._i - 2][pp._j] = '$';
            Node n_up(new_map, &node);
            children.push_back(n_up);
        }
    }

    //Reset the map for the next node:
    new_map = curr_map;

    //Look down:
    if( curr_map[pp._i + 1][pp._j] == '.' || curr_map[pp._i + 1][pp._j] == 'G' ) //If there is either a free spot or goal
    {
        if(pp.in_list(_goal_pos)) //If player is on a goal square, redraw the goalsquare, otherwise player must be on empty square '.'
            new_map[pp._i][pp._j] = 'G';
        else
            new_map[pp._i][pp._j] = '.';
        new_map[pp._i + 1][pp._j] = '@';
        Node n_down(new_map, &node);
        children.push_back(n_down);

    }
    else if( curr_map[pp._i + 1][pp._j] == '$'  ) //If there is a box
    {
        if( curr_map[pp._i + 2][pp._j] == '.' ||  curr_map[pp._i + 2][pp._j] == 'G' ) //Notice +2
        {
            if(pp.in_list(_goal_pos)) //If player is on a goal square, redraw the goalsquare, otherwise player must be on empty square '.'
                new_map[pp._i][pp._j] = 'G';
            else
                new_map[pp._i][pp._j] = '.';
            new_map[pp._i + 1][pp._j] = '@';
            new_map[pp._i + 2][pp._j] = '$';
            Node n_down(new_map, &node);
            children.push_back(n_down);
        }
    }

    //Reset the map for the next node:
    new_map = curr_map;

    //Look left:
    if( curr_map[pp._i][pp._j - 1] == '.' || curr_map[pp._i][pp._j - 1] == 'G' ) //If there is either a free spot or goal
    {
        if(pp.in_list(_goal_pos)) //If player is on a goal square, redraw the goalsquare, otherwise player must be on empty square '.'
            new_map[pp._i][pp._j] = 'G';
        else
            new_map[pp._i][pp._j] = '.';
        new_map[pp._i][pp._j - 1] = '@';
        Node n_left(new_map, &node);
        children.push_back(n_left);

    }
    else if( curr_map[pp._i][pp._j - 1] == '$'  ) //If there is a box
    {
        if( curr_map[pp._i][pp._j - 2] == '.' ||  curr_map[pp._i][pp._j - 2] == 'G' ) //Notice -2
        {
            if(pp.in_list(_goal_pos)) //If player is on a goal square, redraw the goalsquare, otherwise player must be on empty square '.'
                new_map[pp._i][pp._j] = 'G';
            else
                new_map[pp._i][pp._j] = '.';
            new_map[pp._i][pp._j - 1] = '@';
            new_map[pp._i][pp._j - 2] = '$';
            Node n_left(new_map, &node);
            children.push_back(n_left);
        }
    }

    //Reset the map for the next node:
    new_map = curr_map;

    //Look right:
    if( curr_map[pp._i][pp._j + 1] == '.' || curr_map[pp._i][pp._j + 1] == 'G' ) //If there is either a free spot or goal
    {
        if(pp.in_list(_goal_pos)) //If player is on a goal square, redraw the goalsquare, otherwise player must be on empty square '.'
            new_map[pp._i][pp._j] = 'G';
        else
            new_map[pp._i][pp._j] = '.';
        new_map[pp._i][pp._j + 1] = '@';
        Node n_right(new_map, &node);
        children.push_back(n_right);

    }
    else if( curr_map[pp._i][pp._j + 1] == '$'  ) //If there is a box
    {
        if( curr_map[pp._i][pp._j + 2] == '.' ||  curr_map[pp._i][pp._j + 2] == 'G' ) //Notice +2
        {
            if(pp.in_list(_goal_pos)) //If player is on a goal square, redraw the goalsquare, otherwise player must be on empty square '.'
                new_map[pp._i][pp._j] = 'G';
            else
                new_map[pp._i][pp._j] = '.';
            new_map[pp._i][pp._j + 1] = '@';
            new_map[pp._i][pp._j + 2] = '$';
            Node n_right(new_map, &node);
            children.push_back(n_right);
        }
    }

    vector<Node> not_in_CL; //Only return the children not in the closed list
    for(const auto &child : children)
    {
        if(_um.find(child) == _um.end())
            not_in_CL.push_back(child);
    }
    return not_in_CL;
}

void Sokoban::merge(vector<Node> children)
{
    for(int i = 0; i < children.size(); i++)
        _open_list.push(children[i]);           //This just pushes onto the stack DFS/BFS style
}

void Sokoban::merge_w_stack(vector<Node> children, stack<Node> &open_list)
{
    for(int i = 0; i < children.size(); i++)
        open_list.push(children[i]);           //This just pushes onto the stack DFS/BFS style
}


void Sokoban::merge_w_priority_q(vector<Node> children, priority_queue<Node, vector<Node>, ComparisonClass> &open_list)
{
    for(int i = 0; i < children.size(); i++)
        open_list.push(children[i]);           //This just pushes onto the stack DFS/BFS style
}


bool Sokoban::is_goal(Node node)                //Should return path
{
    vector<Position> box_pos = node.get_box_positions();

    //sort(box_pos.begin(), box_pos.end(), comparePositions );

   return equal(box_pos.begin(), box_pos.end(), _goal_pos.begin()); //Works because find_boxes scans one row at a time. So list is sorted already.
}




bool Sokoban::comparePositions(Position p1, Position p2)
{
    //Idea taken from:
    //https://stackoverflow.com/questions/7215804/sorting-coordinate-points-c
    int p1_i_scaled = p1._i * 1000;
    int p2_i_scaled = p2._i * 1000;

    return (p1_i_scaled + p1._j) < (p2_i_scaled + p2._j);
}


vector<Node> Sokoban::expand_try_hard(Node node)
{

}


vector<Node> Sokoban::get_closed_list()
{
    return _closed_list;
}

vector<Position> Sokoban::get_goal_pos()
{
    return _goal_pos;
}


void Sokoban::search_IDFS(Node init, Node &goal)
{
    stack<Node> open_list;
    open_list.push(init);
    _closed_list.reserve(10000); //(8-11-2018 10:47) must be quite big for large mazes
    //queue<Node> open_list;
    Node node;
    vector<Node> children;

    while( !open_list.empty() )
    {
        node = open_list.top();
        open_list.pop();
        _closed_list.push_back(node);

        if( is_goal(node) )
        {
            cout << "I found the goal!!!" << endl;
            goal = node;
            return;
        }
        //children = expand(node);
        if(node.depth() < 14)
        {
            children = expand(_closed_list.back()); //Expand the latest node from CL (to be able to reference)
            merge_w_stack(children, open_list);
        }
        //else
         //   cout << "Too deep" << endl;
    }
}

void Sokoban::search_Astar(Node init, Node &goal)
{
    priority_queue<Node, vector<Node>, ComparisonClass> open_list;
    open_list.push(init);
    _closed_list.reserve(10000000); //(8-11-2018 10:47) must be quite big for large mazes
    //queue<Node> open_list;
    Node node;
    vector<Node> children;

    while( !open_list.empty() )
    {
        node = open_list.top();
        open_list.pop();
        _closed_list.push_back(node);
        _um.insert({node,true});


        if( is_goal(node) )
        {
            cout << "I found the goal!!!" << endl;
            goal = node;
            return;
        }

        children = expand_gh(_closed_list.back()); //Expand the latest node from CL (to be able to reference)
        merge_w_priority_q(children, open_list);
    }
}


vector<Node> Sokoban::expand_gh(Node &node)
{
    vector<Node> children;

    vector<string> curr_map = node.get_map();
    vector<string> new_map = curr_map;

    pos pp = node.get_player_pos();

    //Look up:
    if( curr_map[pp._i - 1][pp._j] == '.' || curr_map[pp._i - 1][pp._j] == 'G' ) //If there is either a free spot or goal
    {
        if(pp.in_list(_goal_pos)) //If player is on a goal square, redraw the goalsquare, otherwise player must be on empty square '.'
            new_map[pp._i][pp._j] = 'G';
        else
            new_map[pp._i][pp._j] = '.';

        new_map[pp._i - 1][pp._j] = '@';
        Node n_up(new_map, &node, _goal_pos);
        children.push_back(n_up);
    }
    else if( curr_map[pp._i - 1][pp._j] == '$'  ) //If there is a box
    {
        if( curr_map[pp._i - 2][pp._j] == '.' ||  curr_map[pp._i - 2][pp._j] == 'G' ) //Notice -2
        {
            if(pp.in_list(_goal_pos)) //If player is on a goal square, redraw the goalsquare, otherwise player must be on empty square '.'
                new_map[pp._i][pp._j] = 'G';
            else
                new_map[pp._i][pp._j] = '.';
            new_map[pp._i - 1][pp._j] = '@';
            new_map[pp._i - 2][pp._j] = '$';
            Node n_up(new_map, &node, _goal_pos);
            children.push_back(n_up);
        }
    }

    //Reset the map for the next node:
    new_map = curr_map;

    //Look down:
    if( curr_map[pp._i + 1][pp._j] == '.' || curr_map[pp._i + 1][pp._j] == 'G' ) //If there is either a free spot or goal
    {
        if(pp.in_list(_goal_pos)) //If player is on a goal square, redraw the goalsquare, otherwise player must be on empty square '.'
            new_map[pp._i][pp._j] = 'G';
        else
            new_map[pp._i][pp._j] = '.';
        new_map[pp._i + 1][pp._j] = '@';
        Node n_down(new_map, &node, _goal_pos);
        children.push_back(n_down);

    }
    else if( curr_map[pp._i + 1][pp._j] == '$'  ) //If there is a box
    {
        if( curr_map[pp._i + 2][pp._j] == '.' ||  curr_map[pp._i + 2][pp._j] == 'G' ) //Notice +2
        {
            if(pp.in_list(_goal_pos)) //If player is on a goal square, redraw the goalsquare, otherwise player must be on empty square '.'
                new_map[pp._i][pp._j] = 'G';
            else
                new_map[pp._i][pp._j] = '.';
            new_map[pp._i + 1][pp._j] = '@';
            new_map[pp._i + 2][pp._j] = '$';
            Node n_down(new_map, &node, _goal_pos);
            children.push_back(n_down);
        }
    }

    //Reset the map for the next node:
    new_map = curr_map;

    //Look left:
    if( curr_map[pp._i][pp._j - 1] == '.' || curr_map[pp._i][pp._j - 1] == 'G' ) //If there is either a free spot or goal
    {
        if(pp.in_list(_goal_pos)) //If player is on a goal square, redraw the goalsquare, otherwise player must be on empty square '.'
            new_map[pp._i][pp._j] = 'G';
        else
            new_map[pp._i][pp._j] = '.';
        new_map[pp._i][pp._j - 1] = '@';
        Node n_left(new_map, &node, _goal_pos);
        children.push_back(n_left);

    }
    else if( curr_map[pp._i][pp._j - 1] == '$'  ) //If there is a box
    {
        if( curr_map[pp._i][pp._j - 2] == '.' ||  curr_map[pp._i][pp._j - 2] == 'G' ) //Notice -2
        {
            if(pp.in_list(_goal_pos)) //If player is on a goal square, redraw the goalsquare, otherwise player must be on empty square '.'
                new_map[pp._i][pp._j] = 'G';
            else
                new_map[pp._i][pp._j] = '.';
            new_map[pp._i][pp._j - 1] = '@';
            new_map[pp._i][pp._j - 2] = '$';
            Node n_left(new_map, &node, _goal_pos);
            children.push_back(n_left);
        }
    }

    //Reset the map for the next node:
    new_map = curr_map;

    //Look right:
    if( curr_map[pp._i][pp._j + 1] == '.' || curr_map[pp._i][pp._j + 1] == 'G' ) //If there is either a free spot or goal
    {
        if(pp.in_list(_goal_pos)) //If player is on a goal square, redraw the goalsquare, otherwise player must be on empty square '.'
            new_map[pp._i][pp._j] = 'G';
        else
            new_map[pp._i][pp._j] = '.';
        new_map[pp._i][pp._j + 1] = '@';
        Node n_right(new_map, &node, _goal_pos);
        children.push_back(n_right);

    }
    else if( curr_map[pp._i][pp._j + 1] == '$'  ) //If there is a box
    {
        if( curr_map[pp._i][pp._j + 2] == '.' ||  curr_map[pp._i][pp._j + 2] == 'G' ) //Notice +2
        {
            if(pp.in_list(_goal_pos)) //If player is on a goal square, redraw the goalsquare, otherwise player must be on empty square '.'
                new_map[pp._i][pp._j] = 'G';
            else
                new_map[pp._i][pp._j] = '.';
            new_map[pp._i][pp._j + 1] = '@';
            new_map[pp._i][pp._j + 2] = '$';
            Node n_right(new_map, &node, _goal_pos);
            children.push_back(n_right);
        }
    }

    vector<Node> not_in_CL; //Only return the children not in the closed list
    for(auto child : children)
    {
        if(_um.find(child) == _um.end())
            not_in_CL.push_back(child);
    }

    return not_in_CL;
}



