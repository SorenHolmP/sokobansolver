#ifndef STATE_H
#define STATE_H

#include <vector>
#include <iostream>
#include "Position.h"


using namespace std;

class State
{
public:
    State();
    State(vector<string> map);
    State(Position player_pos, vector<Position> box_positions);


    vector<string> _map;
    Position _player_pos;
    vector<Position> _box_positions;

};

#endif // STATE_H
