include "help_funcs.h"

Position find_player(vector<string> map)
{
    for(int i = 0; i < map.size(); i++)
    {
        for(int j = 0; j < map[i].size(); j++)
        {
            if(map[i][j] == '@') //Find player
                return Position(i,j);
        }
    }
}

vector<Position> find_boxes(vector<string> map)
{
    vector<Position> box_positions;
    for(int i = 0; i < map.size(); i++)
    {
        for(int j = 0; j < map[i].size(); j++)
        {
            if(map[i][j] == '$') //Find box
                box_positions.push_back(Position(i,j));
        }
    }
    return box_positions;
}

vector<Position> find_goals(vector<string> map)
{
    vector<Position> goal_positions;
    for(int i = 0; i < map.size(); i++)
    {
        for(int j = 0; j < map[i].size(); j++)
        {
            if(map[i][j] == 'G') //Find box
                goal_positions.push_back(Position(i,j));
        }
    }
    return goal_positions;
}

vector<Position> find_free_squares(vector<string> map)
{
    vector<Position> free_positions;
    for(int i = 0; i < map.size(); i++)
    {
        for(int j = 0; j < map[i].size(); j++)
        {
            if(map[i][j] == '.') //Find box
                free_positions.push_back(Position(i,j));
        }
    }
    return free_positions;
}


void print_map(vector<string> map)
{
    for(int i = 0; i < map.size(); i++)
    {
        for(int j = 0; j < map[i].size(); j++)
        {
            cout << map[i][j];
        }
        cout << endl;
    }
}



vector<string> load_level_from_file(string file_path)
{
    vector<string> level;
    ifstream myfile (file_path);
    string line;
    if (myfile.is_open())
    {
        while(getline(myfile,line))
        {
            level.push_back(line);
        }
    }

    return level;

}


//void print_path(Node goal)
//{
//    //while()
//}




bool in_list(pos p, vector<pos> list)
{
    for(int i = 0; i < list.size(); i++)
    {
        if(list[i] == p)
            return true;
    }
    return false;
}


