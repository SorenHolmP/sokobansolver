#ifndef GLOB_DEF_H
#define GLOB_DEF_H


#include "Position.h"

Position UP(-1,0);
Position DOWN(1,0);
Position LEFT(0,-1);
Position RIGHT(0,1);


#endif // GLOB_DEF_H
