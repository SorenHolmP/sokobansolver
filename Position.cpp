#include "Position.h"

Position::Position(){}
Position::Position(int i, int j): _i(i), _j(j) {}

bool Position::operator ==(Position p2)
{
    return p2._i == _i && p2._j == _j;
}

Position Position::operator+(Position p2)
{
    return Position(_i + p2._i, _j + p2._j);
}

Position Position::operator-(Position p2)
{
    return Position(_i - p2._i, _j - p2._j);
}

ostream& operator<<(ostream& os, const Position& pos)
{
    os << "(" << pos._i << "," << pos._j << ")";
}

int Position::manhattan_dist(Position p2)
{
    return abs(_i - p2._i) + abs(_j - p2._j);
}

bool Position::in_list(vector<Position> list)
{
    for(int i = 0; i < list.size(); i++)
    {
        if(list[i] == *this)
            return true;
    }
    return false;
}
