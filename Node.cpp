#include "Node.h"

Node::Node(){}

Node::Node(vector<string> map): _parent(nullptr) //For initializing the seed
{
    _map = map;
    _box_positions  = find_boxes(map);
    _player_pos     = find_player(map);
}

Node::Node(Node *parent): _parent(parent) {}

Node::Node(vector<string> map, Node *parent): _parent(parent)
{
    _map = map;
    _box_positions  = find_boxes(map);
    _player_pos     = find_player(map);
}

Node::Node(vector<string> map, Node *parent, vector<Position> box_goal): _parent(parent)
{
    _map = map;
    _box_positions  = find_boxes(map);
    _player_pos     = find_player(map);
     _h = calc_heuristic(box_goal); //<--terrible...
     _g = depth();
}



Node* Node::get_parent()
{
    return _parent;
}


vector<string> Node::get_map()
{
    return _map;
}

vector<Position> Node::get_box_positions()
{
    return _box_positions;
}
Position Node::get_player_pos()
{
    return _player_pos;
}

int Node::depth()
{
    int i = 0;
    Node *n_ptr = _parent;
    while(n_ptr != nullptr)
    {
        n_ptr = n_ptr->_parent;
        i++;
    }
    return i;
}

int Node::get_g() const
{
    return _g;
}
int Node::get_h() const
{
    return _h;
}

//bool Node::operator==(Node n2)
//{
//    vector<Position> n2_b = n2.get_box_positions();

//    bool box_pos_equal = false, player_pos_equal = false;

//    box_pos_equal = equal(_box_positions.begin(), _box_positions.end(), n2_b.begin());
//    player_pos_equal = _player_pos == n2.get_player_pos();

//    //If boxes and player are in the same position, then the states are identitcal

//    return box_pos_equal && player_pos_equal;
//}


int Node::calc_heuristic(vector<Position> goal_pos)
{
    int h = 0;
    //int sum = 0;

//    int pairdata[_box_positions.size()][goal_pos.size()] = {0};
//    _box_positions;
//    int min_dist = 1000;
//    for(int i = 0; i < _box_positions.size(); i++)
//    {
//        int k = 0;
//        for(int j = 0; j < goal_pos.size(); j++)
//        {
//            if(_box_positions[i].manhattan_dist(goal_pos[j]) < min_dist)
//            {
//                min_dist = _box_positions[i].manhattan_dist(goal_pos[j]);
//                k = j;
//            }
//        }
//        pairdata[i][k] = min_dist;
//    }

//    for(int i = 0; i < _box_positions.size(); i++)
//        for(int j = 0; j < goal_pos.size(); j++)
//            h+= pairdata[i][j];

    //Løb alle box positions igennem
    //->Find nærmeste mål, gem mål

    for(int i = 0; i < _box_positions.size(); i++)
    {
        //h += _player_pos.manhattan_dist(goal_box_pos[i]);

          for(int j = 0; j < _box_positions.size(); j++)
          {
              h += goal_pos[j].manhattan_dist(_box_positions[i]);
          }
        //h += goal_box_pos[i].manhattan_dist(_box_positions[i]); //<--THIS IS WRONG
    }
    return h;
}


//bool Node::in_list(vector<Node> list)
//{
//    for(int i = 0; i < list.size(); i++)
//    {
//        if(list[i] == *this)
//            return true;
//    }
//    return false;
//}


//template<typename T>
//bool Node::template_in_list(T list)
//{
//    cout << "lol";
//}


//Node::Node(State state): _state(state)
//{

//}


//State Node::get_state()
//{
//    return _state;
//}

//void Node::set_state(State state)
//{
//    _state = state;
//}

