#ifndef SOKOBAN_H
#define SOKOBAN_H

#include <iostream>
#include <vector>
#include <stack>
#include <queue>
#include <algorithm>    // std::equal
#include <unordered_map> // Hash table


#include "Position.h"
#include "State.h"
#include "Node.h"
#include "help_funcs.h"



using namespace std;

class Sokoban
{
public:
    Sokoban();
    Sokoban(vector<string> map);

    void search(Node init, Node &goal);                         //Breadth first search
    void search_IDFS(Node init, Node &goal);                    //Iterative deepening
    void search_Astar(Node init, Node &goal);

    vector<Node> expand(Node &node);
    vector<Node> expand_gh(Node &node);
    vector<Node> expand_try_hard(Node node);

    void merge(vector<Node> children);
    void merge_w_stack(vector<Node> children, stack<Node> &open_list);
    void merge_w_priority_q(vector<Node> children, priority_queue<Node, vector<Node>, ComparisonClass> &open_list);



    bool is_goal(Node node);                                   //Should return path (TO BE DONE)

    static bool comparePositions(Position p1, Position p2);    //Comparision function to sort box positions.
                                                               //Static because: https://stackoverflow.com/questions/29286863/invalid-use-of-non-static-member-function...
    vector<Node> get_closed_list();
    vector<Position> get_goal_pos();

private:
    queue<Node> _open_list;   //BFS
    //stack<Node> _open_list; //DFS
    vector<Node> _closed_list;
    unordered_map<Node, bool, MyHash> _um;                    //Hash table


    //_solution
    vector<Position> _goal_pos;                              //Must be sorted upon initialization (TO BE DONE)... <-- unsure, think the initialization of map does this automatically
    vector<string> _map;

};


#endif // SOKOBAN_H
