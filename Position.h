#ifndef POSITION_H
#define POSITION_H

#include <iostream>
#include <vector>
using namespace std;


class Position
{
public:
    Position();
    Position(int i, int j);

    //Operator overloading:
    bool operator==(Position p2);                                   //For comparing points - equal if same coordinates
    Position operator+(Position p2);
    Position operator-(Position p2);

    bool in_list(vector<Position> list);
    int manhattan_dist(Position p2);

    friend ostream& operator<<(ostream& os, const Position& pos);   //For cout'ing point. Taken from https://msdn.microsoft.com/en-us/library/1z2f6c2k.aspx

    int _i,_j;
};

typedef Position pos; //To avoid write 'Position' all the time


#endif // POSITION_H
